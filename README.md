# HDCore - docker-jsonlint

## Introduction

This is a small container image that contains a Node environment with [@prantlf/jsonlint](https://github.com/prantlf/jsonlint) (better version of original [jsonlint](https://github.com/zaach/jsonlint)). This container is very useful for automatic testing during CI.

## Usage

- Run jsonlint with default exclusions or with a [jsonlintrc.\*](https://github.com/prantlf/jsonlint#configuration) configurationfile:

```bash
docker run --rm -v /path/to/code:/code hdcore/docker-jsonlint:<version>
```

- Run jsonlint with custom [options](https://github.com/prantlf/jsonlint#usage):

```bash
docker run --rm -v /path/to/code:/code hdcore/docker-jsonlint:<version> jsonlint.sh <options>
```

- Use in .gitlab-ci.yml:

```yaml
jsonlint:
  stage: lint
  image: hdcore/docker-jsonlint:1
  script:
    - jsonlint.sh
```

- Run shell (debug):

```bash
docker run -it --rm -v /path/to/code:/code hdcore/docker-jsonlint:<version> /bin/sh

docker compose run -it --rm -v /path/to/code:/code local-jsonlint-<version> /bin/sh
```

## Tags

- hdcore/docker-jsonlint:1

## Registries

The image is stored on multiple container registries at DockerHub and Gitlab:

- Docker Hub:
  - hdcore/docker-jsonlint
- Gitlab:
  - registry.gitlab.com/hdcore-docker/docker-jsonlint

## Building

To build the image locally:

```bash
docker build -f <version>/Dockerfile -t docker-jsonlint:<version> .

docker compose build
```
