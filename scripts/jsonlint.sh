#!/bin/sh

# Author: Danny Herpol <hdcore@lachjekrom.com>
# Version GIT: 2023-01-08 11:47

# jsonlint.sh
# for hdcore docker-jsonlint image

if [ "$#" -ne 0 ]
then
    # Pass-trough arguments
    echo 'Pass-trough arguments'
    # shellcheck disable=SC2068 # double quote not allowed due to extra arguments expansion
    jsonlint -ln $@
elif [ -f ".jsonlintrc" ] || [ -f ".jsonlintrc.json" ] || [ -f ".jsonlintrc.yaml" ] || [ -f ".jsonlintrc.yml" ] || [ -f ".jsonlintrc.js" ] || [ -f ".jsonlintrc.cjs" ] || [ -f "jsonlint.config.js" ] || [ -f "jsonlint.config.cjs" ]
then
    # Configuration file present
    echo 'Using configuration file'
    jsonlint -ln
else
    # Default configuration
    echo 'Default configuration'
    jsonlint -ln '**/*.json' '.**/*.json' '!brol/**' '!tmp/**' '!.git/**' '!venv/**' '!**/vendor/**' '!**/__pycache__/**' '!node_modules/**'
fi
